# lib/hangman.rb

class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @referee = players[:referee]
    @guesser = players[:guesser]
  end

  def setup
    word_length = @referee.pick_secret_word
    @board = [nil] * word_length
    @guesser.register_secret_length(word_length)
  end

  def update_board(response, guess)
    response.each do |index|
      @board[index] = guess
    end
  end

  def take_turn
    guess = @guesser.guess
    response = @referee.check_guess(guess)
    update_board(response, guess)
    @guesser.handle_response(response)
  end

  def play
  end
end

class HumanPlayer
  def initialize(dictionary)
    @dictionary = dictionary
  end

  # Referee methods
  def pick_secret_word
    loop do
      print 'Which word do you want?: '
      word = gets.chomp
      if @dictionary.include?(word)
        @secret_word = word
        break
      else
        puts 'Invalid word'
      end
    end
  end

  def check_guess(letter)
    @secret_word.chars.each_index.each_with_object([]) do |index, indices|
      indices << index if @secret_word[index] == letter
    end
  end

  # Guesser methods
  def register_secret_length(length)
    @secret_length = length
  end

  def guess
    print 'Guess a letter: '
    gets.chomp
  end

  def handle_response
  end
end

class ComputerPlayer
  attr_reader :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
    @candidate_words = dictionary
  end

  # Referee methods
  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    @secret_word.chars.each_index.each_with_object([]) do |index, indices|
      indices << index if @secret_word[index] == letter
    end
  end

  # Guesser methods
  def register_secret_length(length)
    @secret_length = length
    @candidate_words = @candidate_words.select { |word| word.length == @secret_length }
  end

  def guess(board)
    (('a'..'z').to_a - board.compact).each_with_object(Hash.new(0)) do |char, hash|
      hash[char] = @candidate_words.join.count(char)
    end.sort_by { |_, v| v }.last.first
  end

  def handle_response(guessed_char, indices)
    @candidate_words = @candidate_words.select do |word|
      word.each_char.with_index.all? do |candidate_word_char, index|
        if indices.include?(index)
          candidate_word_char == guessed_char
        else
          candidate_word_char != guessed_char
        end
      end
    end
  end
end
